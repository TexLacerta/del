﻿using DEL.Infrastructure.Dto;
using DEL.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace DEL.Api.Controllers
{
    public class NewsController : ApiController
    {
        private INewsService _newsService;
        public NewsController(INewsService newsService)
        {
            _newsService = newsService;
        }

        public IHttpActionResult Get(int id)
        {
            return Json(_newsService.Get(id));
        }

        public IHttpActionResult Post([FromBody] NewsDto news)
        {
            _newsService.InsertOrUpdate(news);
            return Json(true);
        }

        public IHttpActionResult Get()
        {
            return Json(_newsService.GetAll());
        }

        public IHttpActionResult Delete(int id)
        {
            _newsService.Remove(id);
            return Json(true);
        }
    }
}