﻿using DEL.Infrastructure.Dto;
using DEL.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace DEL.Api.Controllers
{
    public class PersonEntryController : ApiController
    {
        private IPersonEntryService _personEntryService;
        public PersonEntryController(IPersonEntryService personEntryService)
        {
            _personEntryService = personEntryService;
        }

        public IHttpActionResult Get(int id)
        {
            return Json(_personEntryService.Get(id));
        }

        public IHttpActionResult Post([FromBody] PersonEntryDto news)
        {
            _personEntryService.InsertOrUpdate(news);
            return Json(true);
        }

        public IHttpActionResult Get()
        {
            return Json(_personEntryService.GetAll());
        }

        public IHttpActionResult Delete(int id)
        {
            _personEntryService.Remove(id);
            return Json(true);
        }
    }
}