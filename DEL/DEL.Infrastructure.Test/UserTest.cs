﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DEL.Infrastructure.Dto;
using DEL.Infrastructure.Mappers;

namespace DEL.Infrastructure.Test
{
    [TestClass]
    public class UserTest
    {
        [TestMethod]
        public void GetEntry()
        {
            var services = new Infrastructure.Services.PersonEntryService(AutoMapperConfig.Initialize());
            var user = services.Get(11);
            Assert.AreEqual("Kowalczyk", user.Surname);
        }

        [TestMethod]
        public void GetNews()
        {
            var services = new Infrastructure.Services.NewsService(AutoMapperConfig.Initialize());
            var news = services.Get(4);
            Assert.AreEqual("pracownik", news.Author);
        }

        [TestMethod]
        public void GetEntries()
        {
            var services = new Infrastructure.Services.PersonEntryService(AutoMapperConfig.Initialize());
            var user = services.GetAll();
            Assert.AreEqual("Warszawa", user[0].PlaceOfBirth);
        }
    }
}
