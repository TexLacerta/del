﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEL.Infrastructure.Dto
{
    public class PersonEntryDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Name2 { get; set; }
        public string Surname { get; set; }
        public string FamilySurname { get; set; }

        public DateTime DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }

        public int Gender { get; set; }

        public string PeselNumber { get; set; }
        public string IdCardNumber { get; set; }

        public int FatherId { get; set; }
        public int MotherId { get; set; }

        public bool Death { get; set; }
        public string PlaceOfDeath { get; set; }
        public DateTime? DateOfDeath { get; set; }

        public string AddressCity { get; set; }
        public string AddressPostalCode { get; set; }
        public string AddressPostalCity { get; set; }
        public string AddressHouseNumber { get; set; }
        public string AddressStreetName { get; set; }
        public string AddressAptNumber { get; set; }

        public bool Married { get; set; }
        public int SpouseId { get; set; }
    }
}
