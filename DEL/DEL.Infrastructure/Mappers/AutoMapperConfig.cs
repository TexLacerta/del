﻿using AutoMapper;
using DEL.Infrastructure.Domain;
using DEL.Infrastructure.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEL.Infrastructure.Mappers
{
    public static class AutoMapperConfig
    {
        public static IMapper Initialize()
            => new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PersonEntryDto, PersonEntry>();
                cfg.CreateMap<PersonEntry, PersonEntryDto>();
                cfg.CreateMap<NewsDto, News>();
                cfg.CreateMap<News, NewsDto>();
            }).CreateMapper();
    }
}
