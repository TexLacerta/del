﻿using DEL.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEL.Infrastructure.Repositories
{
    public interface IPersonEntryRepository : IDataRepository<PersonEntry>
    {
    }
}
