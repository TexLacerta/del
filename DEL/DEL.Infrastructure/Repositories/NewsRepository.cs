﻿using Dapper;
using DEL.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEL.Infrastructure.Repositories
{
    public class NewsRepository : INewsRepository
    {
        public News Get(int id)
        {
            News news = null;
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlServerConnString"].ConnectionString))
            {
                db.Open();
                news = db.Query<News>("SELECT * FROM [News] " + "WHERE Id =" + id, new { id }).SingleOrDefault();
            }
            return news;
        }

        public IList<News> GetAll()
        {
            IList<News> news = null;
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlServerConnString"].ConnectionString))
            {
                db.Open();
                news = db.Query<News>("SELECT * FROM [News]").ToList();
            }
            return news;
        }

        public int InsertOrUpdate(News item)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlServerConnString"].ConnectionString))
            {
                db.Open();
                if (item.Id > 0)
                    return Update(item, db);
                else
                    return Insert(item, db);
            }
        }

        private int Insert(News item, IDbConnection db)
        {
            string sql = @"INSERT INTO [News] (Author, Title, Text, Date) Values (@Author, @Title, @Text, @Date); SELECT CAST(SCOPE_IDENTITY() as int)";
            var id = db.Query<int>(sql, new
            {
                Author = item.Author,
                Title = item.Title,
                Text = item.Text,
                Date = item.Date
            }).Single();
            return id;
        }

        private int Update(News item, IDbConnection db)
        {
            const string sql = @"UPDATE [News] SET Author = @Author, Title = @Title, Text = @Text, Date = @Date WHERE Id = @Id;";
            var affectedRows = db.Execute(sql, new
            {
                Id = item.Id,
                Author = item.Author,
                Title = item.Title,
                Text = item.Text,
                Date = item.Date
            });
            return affectedRows;
        }

        public void Remove(int id)
        {
            string sql = "DELETE FROM [News] WHERE Id = @Id";
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlServerConnString"].ConnectionString))
            {
                db.Open();
                var affectedRows = db.Execute(sql, new { Id = id });
            }
        }
    }
}
