﻿using Dapper;
using DEL.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEL.Infrastructure.Repositories
{
    public class PersonEntryRepository : IPersonEntryRepository
    {
        public PersonEntry Get(int id)
        {
            PersonEntry personEntry = null;
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlServerConnString"].ConnectionString))
            {
                db.Open();
                personEntry = db.Query<PersonEntry>("SELECT * FROM [PersonEntry] " + "WHERE Id =" + id, new { id }).SingleOrDefault();
            }
            return personEntry;
        }

        public IList<PersonEntry> GetAll()
        {
            IList<PersonEntry> personEntries = null;
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlServerConnString"].ConnectionString))
            {
                db.Open();
                personEntries = db.Query<PersonEntry>("SELECT * FROM [PersonEntry]").ToList();
            }
            return personEntries;
        }

        public int InsertOrUpdate(PersonEntry item)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlServerConnString"].ConnectionString))
            {
                db.Open();
                if (item.Id > 0)
                    return Update(item, db);
                else
                    return Insert(item, db);
            }
        }

        private int Insert(PersonEntry item, IDbConnection db)
        {
            string sql = @"INSERT INTO [PersonEntry] (Name, Name2, Surname, FamilySurname, DateOfBirth, PlaceOfBirth, Gender, PeselNumber, IdCardNumber, FatherId, MotherId, Death, PlaceOfDeath, DateOfDeath, AddressCity, AddressPostalCode, AddressPostalCity, AddressHouseNumber, AddressStreetName, AddressAptNumber, Married, SpouseId) Values (@Name, @Name2, @Surname, @FamilySurname, @DateOfBirth, @PlaceOfBirth, @Gender, @PeselNumber, @IdCardNumber, @FatherId, @MotherId, @Death, @PlaceOfDeath, @DateOfDeath, @AddressCity, @AddressPostalCode, @AddressPostalCity, @AddressHouseNumber, @AddressStreetName, @AddressAptNumber, @Married, @SpouseId); SELECT CAST(SCOPE_IDENTITY() as int)";
            var id = db.Query<int>(sql, new
            {
                Name = item.Name,
                Name2 = item.Name2,
                Surname = item.Surname,
                FamilySurname = item.FamilySurname,
                DateOfBirth = item.DateOfBirth,
                PlaceOfBirth = item.PlaceOfBirth,
                Gender = item.Gender,
                PeselNumber = item.PeselNumber,
                IdCardNumber = item.IdCardNumber,
                FatherId = item.FatherId,
                MotherId = item.MotherId,
                Death = item.Death,
                PlaceOfDeath = item.PlaceOfDeath,
                DateOfDeath = item.DateOfDeath,
                AddressCity = item.AddressCity,
                AddressPostalCode = item.AddressPostalCode,
                AddressPostalCity = item.AddressPostalCity,
                AddressHouseNumber = item.AddressHouseNumber,
                AddressStreetName = item.AddressStreetName,
                AddressAptNumber = item.AddressAptNumber,
                Married = item.Married,
                SpouseId = item.SpouseId
            }).Single();
            return id;
        }

        private int Update(PersonEntry item, IDbConnection db)
        {
            const string sql = @"UPDATE [PersonEntry] SET Name = @Name, Name2 = @Name2, Surname = @Surname, FamilySurname = @FamilySurname, DateOfBirth = @DateOfBirth, PlaceOfBirth = @PlaceOfBirth, Gender = @Gender, PeselNumber = @PeselNumber, IdCardNumber = @IdCardNumber, FatherId = @FatherId, MotherId = @MotherId, Death = @Death, PlaceOfDeath = @PlaceOfDeath, DateOfDeath = @DateOfDeath, AddressCity = @AddressCity, AddressPostalCode = @AddressPostalCode, AddressPostalCity = @AddressPostalCity, AddressHouseNumber = @AddressHouseNumber, AddressStreetName = @AddressStreetName, AddressAptNumber = @AddressAptNumber, Married = @Married, SpouseId = @SpouseId WHERE Id = @Id;";
            var affectedRows = db.Execute(sql, new
            {
                Id = item.Id,
                Name = item.Name,
                Name2 = item.Name2,
                Surname = item.Surname,
                FamilySurname = item.FamilySurname,
                DateOfBirth = item.DateOfBirth,
                PlaceOfBirth = item.PlaceOfBirth,
                Gender = item.Gender,
                PeselNumber = item.PeselNumber,
                IdCardNumber = item.IdCardNumber,
                FatherId = item.FatherId,
                MotherId = item.MotherId,
                Death = item.Death,
                PlaceOfDeath = item.PlaceOfDeath,
                DateOfDeath = item.DateOfDeath,
                AddressCity = item.AddressCity,
                AddressPostalCode = item.AddressPostalCode,
                AddressPostalCity = item.AddressPostalCity,
                AddressHouseNumber = item.AddressHouseNumber,
                AddressStreetName = item.AddressStreetName,
                AddressAptNumber = item.AddressAptNumber,
                Married = item.Married,
                SpouseId = item.SpouseId
            });
            return affectedRows;
        }

        public void Remove(int id)
        {
            string sql = "DELETE FROM [PersonEntry] WHERE Id = @Id";
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlServerConnString"].ConnectionString))
            {
                db.Open();
                var affectedRows = db.Execute(sql, new { Id = id });
            }
        }
    }
}
