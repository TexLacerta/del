﻿using DEL.Infrastructure.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEL.Infrastructure.Services
{
    public interface INewsService
    {
        NewsDto Get(int id);
        IList<NewsDto> GetAll();
        int InsertOrUpdate(NewsDto item);
        void Remove(int id);
    }
}
