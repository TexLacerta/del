﻿using DEL.Infrastructure.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEL.Infrastructure.Services
{
    public interface IPersonEntryService
    {
        PersonEntryDto Get(int id);
        IList<PersonEntryDto> GetAll();
        int InsertOrUpdate(PersonEntryDto item);
        void Remove(int id);
    }
}
