﻿using AutoMapper;
using DEL.Infrastructure.Domain;
using DEL.Infrastructure.Dto;
using DEL.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEL.Infrastructure.Services
{
    public class NewsService : INewsService
    {
        private INewsRepository _repository;
        private readonly IMapper _mapper;
        public NewsService(IMapper mapper)
        {
            _repository = new NewsRepository();
            _mapper = mapper;
        }

        public NewsDto Get(int id)
        {
            var news = _repository.Get(id);
            return _mapper.Map<NewsDto>(news);
        }

        public IList<NewsDto> GetAll()
        {
            var news = _repository.GetAll();
            return _mapper.Map<IList<NewsDto>>(news);
        }

        public int InsertOrUpdate(NewsDto item)
        {
            var news = _mapper.Map<News>(item);
            return _repository.InsertOrUpdate(news);
        }

        public void Remove(int id)
        {
            _repository.Remove(id);
        }
    }
}
