﻿using AutoMapper;
using DEL.Infrastructure.Domain;
using DEL.Infrastructure.Dto;
using DEL.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEL.Infrastructure.Services
{
    public class PersonEntryService : IPersonEntryService
    {
        private IPersonEntryRepository _repository;
        private readonly IMapper _mapper;
        public PersonEntryService(IMapper mapper)
        {
            _repository = new PersonEntryRepository();
            _mapper = mapper;
        }

        public PersonEntryDto Get(int id)
        {
            var personEntry = _repository.Get(id);
            return _mapper.Map<PersonEntryDto>(personEntry);
        }

        public IList<PersonEntryDto> GetAll()
        {
            var personEntries = _repository.GetAll();
            return _mapper.Map<IList<PersonEntryDto>>(personEntries);
        }

        public int InsertOrUpdate(PersonEntryDto item)
        {
            var personEntry = _mapper.Map<PersonEntry>(item);
            return _repository.InsertOrUpdate(personEntry);
        }

        public void Remove(int id)
        {
            _repository.Remove(id);
        }
    }
}
