﻿using System;
using DEL.UI.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DEL.UI.Test
{
    [TestClass]
    public class PeselTest
    {
        [TestMethod]
        public void TestPeselGender()
        {
            PeselValid target = new PeselValid();
            PrivateObject obj = new PrivateObject(target);
            var retVal = obj.Invoke("ValidateGender", new object[] { "12220548385", 1 });
            var expectedVal = true;
            Assert.AreEqual(expectedVal, retVal);
        }

        [TestMethod]
        public void TestPeselDate()
        {
            PeselValid target = new PeselValid();
            PrivateObject obj = new PrivateObject(target);
            var retVal = obj.Invoke("ValidateDateOfBirth", new object[] { "12220548385", new DateTime(2012, 2, 5) });
            var expectedVal = true;
            Assert.AreEqual(expectedVal, retVal);
        }

        [TestMethod]
        public void TestPeselChecksum()
        {
            PeselValid target = new PeselValid();
            PrivateObject obj = new PrivateObject(target);
            var retVal = obj.Invoke("ValidateChecksum", new object[] { "12220548389" });
            var expectedVal = true;
            Assert.AreEqual(expectedVal, retVal);
        }

        [TestMethod]
        public void TestPeselGenderFail()
        {
            PeselValid target = new PeselValid();
            PrivateObject obj = new PrivateObject(target);
            var retVal = obj.Invoke("ValidateGender", new object[] { "12220548385", 0 });
            var expectedVal = false;
            Assert.AreEqual(expectedVal, retVal);
        }

        [TestMethod]
        public void TestPeselDateFail()
        {
            PeselValid target = new PeselValid();
            PrivateObject obj = new PrivateObject(target);
            var retVal = obj.Invoke("ValidateDateOfBirth", new object[] { "12220548385", new DateTime(1912, 2, 5) });
            var expectedVal = false;
            Assert.AreEqual(expectedVal, retVal);
        }

        [TestMethod]
        public void TestPeselChecksumFail()
        {
            PeselValid target = new PeselValid();
            PrivateObject obj = new PrivateObject(target);
            var retVal = obj.Invoke("ValidateChecksum", new object[] { "12220548386" });
            var expectedVal = false;
            Assert.AreEqual(expectedVal, retVal);
        }




    }
}
