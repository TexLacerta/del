﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Security.Principal;
using System.Threading;
using System.Web;

namespace DEL.UI.Authentication
{
    /// <summary>
    /// Jednostka, poszukująca ustawienia "roles:{nazwa_użytkownika}" i wnioskująca na jej podstawie role. Role rozdzielone znakami |.
    /// </summary>
    public class SimpleConfigurationPrincipal : IPrincipal
    {
        HashSet<string> _roles = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Inicjalizuje nową instancję klasy jednostki.
        /// </summary>
        /// <param name="identity">Tożsamość.</param>
        public SimpleConfigurationPrincipal(IIdentity identity)
        {
            if (identity == null)
            {
                throw new ArgumentNullException(nameof(identity));
            }

            this.Identity = identity;

            string roleSetting = $"roles:{identity.Name}";
            var roles = ConfigurationManager.AppSettings[roleSetting];

            if (!string.IsNullOrEmpty(roles))
            {
                foreach (var role in roles?.Split('|'))
                {
                    _roles.Add(role);
                }
            }
        }

        /// <summary>
        /// Tożsamość.
        /// </summary>
        public IIdentity Identity
        {
            get;
            private set;
        }

        /// <summary>
        /// Sprawdza, czy użytkownik ma odpowiednią rolę.
        /// </summary>
        /// <param name="role">Rola.</param>
        /// <returns>true jeżeli użytkownik posiada rolę, false jeśli nie.</returns>
        public bool IsInRole(string role)
        {
            return _roles.Contains(role);
        }

        /// <summary>
        /// Ustawia zautentykowaną jednostkę.
        /// </summary>
        /// <param name="principal">Jednostka.</param>
        public static void SetAuthenticatedPrincipal(IPrincipal principal)
        {
            if (principal == null || principal.Identity == null || !principal.Identity.IsAuthenticated || String.IsNullOrEmpty(principal.Identity.Name))
            {
                return;
            }

            if (!(principal is SimpleConfigurationPrincipal))
            {
                principal = new SimpleConfigurationPrincipal(principal.Identity);
            }

            if (HttpContext.Current != null)
            {
                HttpContext.Current.User = principal;
            }

            Thread.CurrentPrincipal = principal;
        }
    }
}