﻿using DEL.UI.Logic;
using DEL.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DEL.UI.Controllers
{
    public class ChartController : Controller
    {
        public ViewResult Index()
        {
            return View();
        }

        public ViewResult Type1()
        {
            return View(new ChartModel());
        }

        public ViewResult Type2()
        {
            return View(new ChartModel());
        }

        public ViewResult Type3()
        {
            return View(new ChartModel());
        }
    }
}