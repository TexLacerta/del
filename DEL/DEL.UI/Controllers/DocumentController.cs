﻿using DEL.UI.Logic;
using DEL.UI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DEL.UI.Controllers
{
    public class DocumentController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Results(string searchString)
        {
            var personEntries = new ApiClient().GetData<List<Entry>>("api/personentry");
            var filteredData = new List<Entry>();

            if (searchString == null)
                RedirectToAction("Index", "Document");

            foreach (var personEntry in personEntries)
            {
                if (Contains(searchString, personEntry))
                    filteredData.Add(personEntry);
            }

            return View(filteredData);
        }

        public ViewResult Birth(Entry model)
        {
            return View(model);
        }

        public void SaveBirthCert(Entry model)
        {
            DocumentEntry docEntry = new DocumentEntry(model);

            MemoryStream file = PdfGenerator.CreatePdf(1, docEntry);
            SaveCert(file);
        }

        public void SaveDeathCert(Entry model)
        {
            DocumentEntry docEntry = new DocumentEntry(model);

            MemoryStream file = PdfGenerator.CreatePdf(2, docEntry);
            SaveCert(file);
        }

        public void SaveCert(MemoryStream file)
        {
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment;filename=odpis-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf");
            Response.OutputStream.Write(file.GetBuffer(), 0, file.GetBuffer().Length);
            Response.OutputStream.Flush();
            Response.OutputStream.Close();
            Response.End();
        }

        public ViewResult Death(Entry model)
        {
            return View(model);
        }

        /// <summary>
        /// Sprawdza, czy szukany tekst zawiera się w danym modelu wpisu osobowego.
        /// </summary>
        /// <param name="searchString">Szukany tekst.</param>
        /// <param name="checkedEntry">Sprawdzany wpis.</param>
        /// <returns></returns>
        private bool Contains(string searchString, Entry checkedEntry)
        {
            if (checkedEntry.Name.Contains(searchString) || checkedEntry.Surname.Contains(searchString) || checkedEntry.PeselNumber.Contains(searchString))
                return true;
            return false;
        }
    }
}