﻿using DEL.UI.Logic;
using DEL.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DEL.UI.Controllers
{
    public class EntryController : Controller
    {
        public ViewResult Index()
        {
            var data = new ApiClient().GetData<List<Entry>>("api/personentry");
            return View(data);
        }

        public ActionResult Search()
        {
            return View();
        }

        public ActionResult Results(string searchString)
        {
            var personEntries = new ApiClient().GetData<List<Entry>>("api/personentry");
            var filteredData = new List<Entry>();

            if (searchString == null)
                RedirectToAction("Search", "Entry");

            foreach (var personEntry in personEntries)
            {
                if (Contains(searchString, personEntry))
                    filteredData.Add(personEntry);
            }

            return View(filteredData);
        }

        [HttpGet]
        public ActionResult Add()
        {
            PopulateRelationshipLists();
            return View();
        }

        [HttpPost]
        public ActionResult Add(Entry model, string femaleList, string maleList, string spouseList)
        {
            model.MotherId = int.Parse(femaleList);
            model.FatherId = int.Parse(maleList);

            if (model.Married)
                model.SpouseId = int.Parse(spouseList);
            else
                model.SpouseId = 0;

            if (!model.Death)
            {
                model.PlaceOfDeath = null;
                model.DateOfDeath = null;
            }

            if (ModelState.IsValid)
            {
                var result = new ApiClient().PostData<Entry>("api/personentry", model);
                return RedirectToAction("Index");
            }
            PopulateRelationshipLists();
            return View(model);
        }

        [HttpGet]
        public ViewResult Delete(Entry model)
        {
            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteAction(Entry model)
        {
            bool result = new ApiClient().DeleteData("api/personentry", model);
            return RedirectToAction("Index", "Entry");
        }

        public ActionResult Edit(Entry model)
        {
            if (model.Id == 0)
                return RedirectToAction("Search", "Entry");
            PopulateRelationshipLists();
            return View(model);
        }

        /// <summary>
        /// Wypełnia listy ojców, matek oraz partnerów w formularzu edycyjnym.
        /// </summary>
        private void PopulateRelationshipLists()
        {
            int currentFatherId = 0;
            int currentMotherId = 0;
            int currentSpouseId = 0;
            int gender = -1;

            if (ModelState.ContainsKey("FatherId"))
            {
                currentFatherId = int.Parse(ModelState["FatherId"].Value.AttemptedValue);
                currentMotherId = int.Parse(ModelState["MotherId"].Value.AttemptedValue);
                currentSpouseId = int.Parse(ModelState["SpouseId"].Value.AttemptedValue);
                gender = int.Parse(ModelState["Gender"].Value.AttemptedValue);
            }
            else if (ModelState.ContainsKey("maleList"))
            {
                currentFatherId = int.Parse(ModelState["maleList"].Value.AttemptedValue);
                currentMotherId = int.Parse(ModelState["femaleList"].Value.AttemptedValue);
                currentSpouseId = int.Parse(ModelState["spouseList"].Value.AttemptedValue);
                gender = int.Parse(ModelState["Gender"].Value.AttemptedValue);
            }

            var personEntries = new ApiClient().GetData<List<Entry>>("api/personentry");
            var maleList = new List<SelectListItem>() { new SelectListItem { Selected = true, Value = "0", Text = "--- nieznany ---" } };
            var femaleList = new List<SelectListItem>() { new SelectListItem { Selected = true, Value = "0", Text = "--- nieznana ---" } };
            var spouseList = new List<SelectListItem>();

            foreach (var personEntry in personEntries)
            {
                if (personEntry.Gender == 0)
                {
                    maleList.Add(new SelectListItem { Selected = (personEntry.Id == currentFatherId), Value = personEntry.Id.ToString(), Text = personEntry.Surname + " " + personEntry.Name + " " + personEntry.Name2 + " (" + personEntry.PeselNumber + ")" });
                }
                else
                {
                    femaleList.Add(new SelectListItem { Selected = (personEntry.Id == currentMotherId), Value = personEntry.Id.ToString(), Text = personEntry.Surname + " " + personEntry.Name + " " + personEntry.Name2 + " (" + personEntry.PeselNumber + ")" });
                }

                if (gender == -1)
                    spouseList.Add(new SelectListItem { Selected = (personEntry.Id == currentSpouseId), Value = personEntry.Id.ToString(), Text = personEntry.Surname + " " + personEntry.Name + " " + personEntry.Name2 + " (" + personEntry.PeselNumber + ")" });
                else if (personEntry.Gender == (1 - gender))
                    spouseList.Add(new SelectListItem { Selected = (personEntry.Id == currentSpouseId), Value = personEntry.Id.ToString(), Text = personEntry.Surname + " " + personEntry.Name + " " + personEntry.Name2 + " (" + personEntry.PeselNumber + ")" });
            }

            ViewBag.MaleList = maleList.OrderBy(x => x.Text);
            ViewBag.FemaleList = femaleList.OrderBy(x => x.Text);
            ViewBag.SpouseList = spouseList.OrderBy(x => x.Text);
        }

        /// <summary>
        /// Sprawdza, czy szukany tekst zawiera się w danym modelu wpisu osobowego.
        /// </summary>
        /// <param name="searchString">Szukany tekst.</param>
        /// <param name="checkedEntry">Sprawdzany wpis.</param>
        /// <returns></returns>
        private bool Contains(string searchString, Entry checkedEntry)
        {
            if (checkedEntry.Name.Contains(searchString) || checkedEntry.Surname.Contains(searchString) || checkedEntry.PeselNumber.Contains(searchString))
                return true;
            return false;
        }
    }
}