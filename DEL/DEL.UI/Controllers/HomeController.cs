﻿using DEL.UI.Logic;
using DEL.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DEL.UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var news = new ApiClient().GetData<List<News>>("api/news");
            var filteredNews = news.Where(x => x.Date > DateTime.Now.AddDays(-14)).Reverse().ToList();

            return View(filteredNews);
        }
    }
}