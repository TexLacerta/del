﻿using DEL.UI.Authentication;
using DEL.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace DEL.UI.Controllers
{
    /// <summary>
    /// Kontroler do logowania.
    /// </summary>
    public class LoginController : Controller
    {
        /// <summary>
        /// Otwiera stronę logowania.
        /// </summary>
        public virtual ActionResult Index()
        {
            return View(new Login());
        }

        /// <summary>
        /// Loguje użytkownika.
        /// </summary>
        /// <param name="model">Model.</param>
        [HttpPost]
        public virtual ActionResult Index(Login model)
        {
            Authenticate(model);
            return View(model);
        }

        /// <summary>
        /// Autentykuje przekazany model.
        /// </summary>
        /// <param name="model">Model.</param>
        /// <param name="redirect">Jeżeli true, przekierowuje po zalogowaniu użytkownika na stronę główną.</param>
        /// <returns>true jeśli model zostaje zautentykowany, w przeciwnym razie false.</returns>
        protected virtual bool Authenticate(Login model, bool redirect = true)
        {
            bool authenticated = FormsAuthentication.Authenticate(model.UserName, model.Password);

            if (!authenticated)
            {
                return false;
            }

            var user = new GenericIdentity(model.UserName);
            var principal = new SimpleConfigurationPrincipal(user);
            SimpleConfigurationPrincipal.SetAuthenticatedPrincipal(principal);

            if (redirect)
            {
                FormsAuthentication.RedirectFromLoginPage(model.UserName, false);
            }

            return true;
        }

        /// <summary>
        /// Wylogowuje użytkownika.
        /// </summary>
        public virtual ActionResult Logoff()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
            return RedirectToAction("Index");
        }
    }
}