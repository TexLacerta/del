﻿using DEL.UI.Logic;
using DEL.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DEL.UI.Controllers
{
    public class NewsController : Controller
    {
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(News model)
        {
            model.Date = DateTime.Now;
            model.Author = System.Web.HttpContext.Current.User.Identity.Name;

            if (ModelState.IsValid)
            {
                var result = new ApiClient().PostData<News>("api/news", model);
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        [HttpGet]
        public ViewResult Delete(News model)
        {
            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteAction(News model)
        {
            bool result = new ApiClient().DeleteData("api/news", model);
            return RedirectToAction("Index", "Home");
        }
    }
}