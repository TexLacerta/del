﻿using DEL.UI.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DEL.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        //adds principal to app
        protected void Application_OnPostAuthenticateRequest(object sender, EventArgs e)
        {
            var user = HttpContext.Current.User;
            SimpleConfigurationPrincipal.SetAuthenticatedPrincipal(user);
        }
    }
}
