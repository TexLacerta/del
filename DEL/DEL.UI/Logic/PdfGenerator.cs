﻿using DEL.UI.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace DEL.UI.Logic
{
    public static class PdfGenerator
    {
        public static BaseFont Font = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);

        public static MemoryStream CreatePdf(int certType, DocumentEntry model)
        {
            MemoryStream workStream = new MemoryStream();
            string fileName = string.Format("odpis-" + "43234323431" + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf");
            string imageFilePath = HttpContext.Current.Server.MapPath("~/Content/img/watermark.png");

            Image background = Image.GetInstance(imageFilePath);
            background.Alignment = Image.UNDERLYING;
            background.ScaleToFit(1000, 1000);

            int partNumber = 1;

            Document doc = new Document();

            PdfWriter.GetInstance(doc, workStream).CloseStream = false;
            doc.Open();

            doc.Add(background);
            doc.Add(new Paragraph("Rzeczpospolita Polska", new Font(Font, 24, 3, BaseColor.BLACK)));
            doc.Add(new Paragraph("Dział Ewidencji Ludności", new Font(Font, 14, 1, BaseColor.BLACK)));
            doc.Add(new Paragraph("\n"));

            if (certType == 1)
            {
                doc.Add(new Paragraph("Odpis skrócony aktu urodzenia", new Font(Font, 18, 1, BaseColor.BLACK)));
                doc.Add(new LineSeparator(3f, 100f, BaseColor.LIGHT_GRAY, 0, -5f));
                doc.Add(new Paragraph("\n"));

                AddHeading(doc, 1, "Dane osobowe");
                PdfPTable table1 = CreateTable(new string[] { "Imię", "Drugie imię", "Nazwisko", "Płeć", "Data urodzenia", "Miejsce urodzenia", "Kraj urodzenia" },
                    new string[] { model.Name, model.Name2, model.Surname, model.Gender, model.DateOfBirth.ToShortDateString(), model.PlaceOfBirth, "Polska" });
                doc.Add(table1);

                AddHeading(doc, 2, "Dane rodziców");
                PdfPTable table2 = CreateTable(new string[] { "", "Imię", "Nazwisko rodowe" }, new string[] { "Ojciec", model.FatherName, model.FatherFamilyName }, new string[] { "Matka", model.MotherName, model.MotherFamilyName });
                doc.Add(table2);

                partNumber = 3;
            }

            if (certType == 2)
            {
                doc.Add(new Paragraph("Odpis skrócony aktu zgonu", new Font(Font, 18, 1, BaseColor.BLACK)));
                doc.Add(new LineSeparator(3f, 100f, BaseColor.LIGHT_GRAY, 0, -5f));
                doc.Add(new Paragraph("\n"));

                AddHeading(doc, 1, "Dane osobowe");
                PdfPTable table1 = CreateTable(new string[] { "Imię", "Drugie imię", "Nazwisko", "Nazwisko rodowe", "Płeć", "Stan cywilny", "Data urodzenia", "Miejsce urodzenia" },
                    new string[] { model.Name, model.Name2, model.Surname, model.FamilySurname, model.Gender, model.MaritalStatus, model.DateOfBirth.ToShortDateString(), model.PlaceOfBirth });
                doc.Add(table1);

                AddHeading(doc, 2, "Dane o zgonie");
                PdfPTable table2 = CreateTable(new string[] { "Data zgonu", "Miejsce zgonu" }, new string[] { model.DateOfDeath.Value.ToShortDateString(), model.PlaceOfDeath });
                doc.Add(table2);

                AddHeading(doc, 3, "Dane małżonka");
                PdfPTable table3 = CreateTable(new string[] { "Imię", "Nazwisko", "Nazwisko rodowe" }, new string[] { model.SpouseName, model.SpouseSurname, model.SpouseFamilyName });
                doc.Add(table3);

                AddHeading(doc, 4, "Dane rodziców");
                PdfPTable table4 = CreateTable(new string[] { "", "Imię", "Nazwisko rodowe" }, new string[] { "Ojciec", model.FatherName, model.FatherFamilyName }, new string[] { "Matka", model.MotherName, model.MotherFamilyName });
                doc.Add(table4);

                partNumber = 5;
            }

            AddHeading(doc, partNumber++, "Dane o odpisie");
            PdfPTable tablea = CreateTable(new string[] { "Data wystawienia", "Godzina wystawienia" }, new string[] { DateTime.Now.ToShortDateString(), DateTime.Now.ToString("HH:mm:ss") });
            doc.Add(tablea);

            AddHeading(doc, partNumber++, "Pieczęć i podpis");
            doc.Add(new LineSeparator(2f, 35f, BaseColor.LIGHT_GRAY, 0, -55f));
            doc.Add(new Paragraph("\n\n\n                               (pieczęć i podpis)", new Font(Font, 8, 3, BaseColor.LIGHT_GRAY)));

            doc.Close();

            return workStream;
        }

        private static void AddKeyCell(PdfPTable table, string cellText)
        {
            table.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font, 10, 1, BaseColor.BLACK)))
            {
                HorizontalAlignment = Element.ALIGN_RIGHT,
                Padding = 5,
                BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255, 0),
                BorderWidth = 0,

            });
        }

        private static void AddValueCell(PdfPTable table, string cellText)
        {
            table.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font, 10, 0, BaseColor.BLACK)))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Padding = 5,
                BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255, 0),
                BorderWidth = 0
            });
        }

        private static void AddHeading(Document doc, int partNumber, string text)
        {
            doc.Add(new Paragraph(partNumber + ". " + text, new Font(Font, 14, 3, BaseColor.BLACK)));
            doc.Add(new LineSeparator(1f, 100f, BaseColor.LIGHT_GRAY, 0, -5f));
            doc.Add(new Paragraph("\n"));
        }

        private static PdfPTable CreateTable(string[] keys, string[] values)
        {
            PdfPTable table = new PdfPTable(new float[] { 30f, 70f });
            for (int i = 0; i < keys.Length; i++)
            {
                AddKeyCell(table, keys[i]);
                AddValueCell(table, values[i]);
            }

            return table;
        }

        private static PdfPTable CreateTable(string[] keys, string[] values1, string[] values2)
        {
            PdfPTable table = new PdfPTable(new float[] { 30f, 35f, 35f });
            for (int i = 0; i < keys.Length; i++)
            {
                AddKeyCell(table, keys[i]);
                AddValueCell(table, values1[i]);
                AddValueCell(table, values2[i]);
            }

            return table;
        }
    }
}