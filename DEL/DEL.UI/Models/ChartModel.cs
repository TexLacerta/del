﻿using DEL.UI.Logic;
using MvcValidationExtensions.Attribute;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;

namespace DEL.UI.Models
{
    public class ChartModel
    {
        public ChartModel()
        {
            var entries = new ApiClient().GetData<List<Entry>>("api/personentry");
            var fEntries = entries.Where(x => x.Death == false).ToArray();

            //type 1
            AgeArray = new string[21];
            PopulationByAge = new int[21];

            for (int i = 0; i < 21; i++)
            {
                AgeArray[i] = i * 5 + "-" + ((i * 5) + 4);
            }
            AgeArray[20] = "100+";

            int now = int.Parse(DateTime.Now.ToString("yyyyMMdd"));

            foreach (var entry in fEntries)
            {
                int dob = int.Parse(entry.DateOfBirth.ToString("yyyyMMdd"));
                int age = (now - dob) / 10000;
                if (age > 100)
                    age = 100;
                double index = age / 5;

                PopulationByAge[(int)Math.Floor(index)]++;
            }

            //type 2
            YearArray = new string[110];
            BirthsByYear = new int[110];
            DeathsByYear = new int[110];

            for (int i = 0; i < 110; i++)
            {
                YearArray[i] = (DateTime.Now.Year - i).ToString();
            }

            foreach (var entry in entries)
            {
                BirthsByYear[DateTime.Now.Year - entry.DateOfBirth.Year]++;
                if (entry.Death)
                    DeathsByYear[DateTime.Now.Year - entry.DateOfDeath.Value.Year]++;
            }

            //type 3
            Population = fEntries.Count();
            Males = fEntries.Where(x => x.Gender == 0).Count();
            Females = fEntries.Where(x => x.Gender == 1).Count();
            Free = fEntries.Where(x => x.Married == false).Count();
            Married = fEntries.Where(x => x.Married == true).Count();

            DateTime y = DateTime.Now.AddYears(-18);
            DateTime z = DateTime.Now.AddYears(-65);

            Young = fEntries.Where(x => x.DateOfBirth >= DateTime.Now.AddYears(-18)).Count();
            Middle = fEntries.Where(x => (x.DateOfBirth <= DateTime.Now.AddYears(-18) && x.DateOfBirth >= DateTime.Now.AddYears(-65))).Count();
            Old = fEntries.Where(x => x.DateOfBirth <= DateTime.Now.AddYears(-65)).Count();
        }

        //type 1
        public int[] PopulationByAge { get; set; }
        public string[] AgeArray { get; set; }

        //type 2
        public int[] BirthsByYear { get; set; }
        public int[] DeathsByYear { get; set; }
        public string[] YearArray { get; set; }

        //type 3
        public int Population { get; set; }

        public int Males { get; set; }
        public int Females { get; set; }

        public int Free { get; set; }
        public int Married { get; set; }

        public int Young { get; set; }
        public int Middle { get; set; }
        public int Old { get; set; }
    }
}