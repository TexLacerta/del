﻿using DEL.UI.Logic;
using MvcValidationExtensions.Attribute;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DEL.UI.Models
{
    public class DocumentEntry
    {
        public DocumentEntry(Entry model)
        {
            Id = model.Id;
            Name = model.Name;
            Name2 = model.Name2;
            Surname = model.Surname;
            FamilySurname = model.FamilySurname;
            DateOfBirth = model.DateOfBirth;
            PlaceOfBirth = model.PlaceOfBirth;

            if (model.Gender == 0)
                Gender = "mężczyzna";
            else
                Gender = "kobieta";

            if (model.Married == false)
            {
                MaritalStatus = "wolny";
                SpouseName = "nie dotyczy";
                SpouseFamilyName = "nie dotyczy";
                SpouseSurname = "nie dotyczy";

            }
            else if (model.Married == true)
            {
                Entry spouse = new ApiClient().GetData<Entry>("api/personentry/" + model.SpouseId);
                SpouseName = spouse.Name + " " + spouse.Name2;
                SpouseFamilyName = spouse.FamilySurname;
                SpouseSurname = spouse.Surname;

                if (model.Gender == 0)
                    MaritalStatus = "żonaty";
                else
                    MaritalStatus = "zamężna";
            }

            if (model.FatherId != 0)
            {
                Entry father = new ApiClient().GetData<Entry>("api/personentry/" + model.FatherId);
                FatherName = father.Name + " " + father.Name2;
                FatherFamilyName = father.FamilySurname;
            }
            else
            {
                FatherName = "nieznane";
                FatherFamilyName = "nieznane";
            }

            if (model.MotherId != 0)
            {
                Entry mother = new ApiClient().GetData<Entry>("api/personentry/" + model.MotherId);
                MotherName = mother.Name + " " + mother.Name2;
                MotherFamilyName = mother.FamilySurname;
            }
            else
            {
                MotherName = "nieznane";
                MotherFamilyName = "nieznane";
            }

            PlaceOfDeath = model.PlaceOfDeath;
            DateOfDeath = model.DateOfDeath;
        }

        public int Id { get; set; }

        [Required]
        [Display(Name = "Imię")]
        [MaxLength(100)]
        [MinLength(3)]
        public string Name { get; set; }

        [Display(Name = "Drugie imię")]
        [MaxLength(100)]
        [MinLength(3)]
        public string Name2 { get; set; }

        [Required]
        [Display(Name = "Nazwisko")]
        [MaxLength(100)]
        [MinLength(3)]
        public string Surname { get; set; }

        [Required]
        [Display(Name = "Nazwisko rodowe")]
        [MaxLength(100)]
        [MinLength(3)]
        public string FamilySurname { get; set; }

        [Required]
        [Display(Name = "Data urodzenia")]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [Display(Name = "Miejsce urodzenia")]
        [MaxLength(100)]
        [MinLength(3)]
        public string PlaceOfBirth { get; set; }

        [Required]
        [Display(Name = "Płeć")]
        public string Gender { get; set; }

        [Display(Name = "Imię ojca")]
        public string FatherName { get; set; }

        [Display(Name = "Nazwisko rodowe ojca")]
        public string FatherFamilyName { get; set; }

        [Display(Name = "Imię matki")]
        public string MotherName { get; set; }

        [Display(Name = "Nazwisko rodowe matki")]
        public string MotherFamilyName { get; set; }

        [Display(Name = "Stan")]
        public string MaritalStatus { get; set; }

        [Display(Name = "Imię małżonka")]
        public string SpouseName { get; set; }

        [Display(Name = "Nazwisko rodowe małżonka")]
        public string SpouseFamilyName { get; set; }

        [Display(Name = "Nazwisko małżonka")]
        public string SpouseSurname { get; set; }

        [Display(Name = "Miejsce śmierci")]
        [MaxLength(100)]
        [MinLength(3)]
        public string PlaceOfDeath { get; set; }

        [Display(Name = "Data zgonu")]
        [DataType(DataType.Date)]
        public DateTime? DateOfDeath { get; set; }
    }
}