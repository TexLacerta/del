﻿using DEL.UI.Logic;
using MvcValidationExtensions.Attribute;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DEL.UI.Models
{
    public class Entry : Entity
    {
        [Required]
        [Display(Name = "Imię")]
        [MaxLength(100)]
        [MinLength(3)]
        public string Name { get; set; }

        [Display(Name = "Drugie imię")]
        [MaxLength(100)]
        [MinLength(3)]
        public string Name2 { get; set; }

        [Required]
        [Display(Name = "Nazwisko")]
        [MaxLength(100)]
        [MinLength(3)]
        public string Surname { get; set; }

        [Required]
        [Display(Name = "Nazwisko rodowe")]
        [MaxLength(100)]
        [MinLength(3)]
        public string FamilySurname { get; set; }

        [Required]
        [Display(Name = "Data urodzenia")]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [Display(Name = "Miejsce urodzenia")]
        [MaxLength(100)]
        [MinLength(3)]
        public string PlaceOfBirth { get; set; }

        [Required]
        [Display(Name = "Płeć")]
        public int Gender { get; set; }

        [Required]
        [Display(Name = "Numer PESEL")]
        [PeselValid]
        public string PeselNumber { get; set; }

        [Display(Name = "Numer dowodu osobistego")]
        [MaxLength(9)]
        [MinLength(9)]
        public string IdCardNumber { get; set; }

        [Required]
        [Display(Name = "Ojciec")]
        public int FatherId { get; set; }

        [Required]
        [Display(Name = "Matka")]
        public int MotherId { get; set; }

        [Required]
        [Display(Name = "Śmierć")]
        public bool Death { get; set; }

        [Display(Name = "Miejsce śmierci")]
        [MaxLength(100)]
        [MinLength(3)]
        public string PlaceOfDeath { get; set; }

        [Display(Name = "Data zgonu")]
        [DataType(DataType.Date)]
        public DateTime? DateOfDeath { get; set; }

        [Display(Name = "Miejscowość")]
        [MaxLength(100)]
        [MinLength(3)]
        public string AddressCity { get; set; }

        [Display(Name = "Kod pocztowy")]
        [MaxLength(5)]
        [MinLength(5)]
        public string AddressPostalCode { get; set; }

        [Display(Name = "Poczta")]
        [MaxLength(100)]
        [MinLength(3)]
        public string AddressPostalCity { get; set; }

        [Display(Name = "Numer domu")]
        [MaxLength(6)]
        [MinLength(1)]
        public string AddressHouseNumber { get; set; }

        [Display(Name = "Nazwa ulicy")]
        [MaxLength(100)]
        [MinLength(3)]
        public string AddressStreetName { get; set; }

        [Display(Name = "Numer mieszkania")]
        [MaxLength(6)]
        [MinLength(1)]
        public string AddressAptNumber { get; set; }

        [Display(Name = "Zamężny")]
        public bool Married { get; set; }

        [Display(Name = "Małżonek")]
        public int SpouseId { get; set; }
    }
}