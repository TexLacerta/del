﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DEL.UI.Models
{
    public class News : Entity
    {
        [Display(Name = "Autor")]
        [MaxLength(100)]
        [MinLength(1)]
        public string Author { get; set; }

        [Required]
        [Display(Name = "Tytuł")]
        [MaxLength(100)]
        [MinLength(1)]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Tekst wiadomości")]
        [MaxLength(1000)]
        [MinLength(1)]
        public string Text { get; set; }

        [Display(Name = "Data zamieszczenia")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
    }
}