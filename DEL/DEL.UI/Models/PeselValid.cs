﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DEL.UI.Logic
{
    /// <summary>
    /// Podejmuje decyzję na temat poprawności numeru PESEL.
    /// </summary>
    public class PeselValid : ValidationAttribute
    {
        /// <summary>
        /// Podejmuje decyzję na temat poprawności numeru PESEL.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns>Odpowiedni obiekt ValidationResult, odpowiadający rezultatowi sprawdzania.</returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var model = (Models.Entry)validationContext.ObjectInstance;
            string _pesel = Convert.ToString(value);
            DateTime _dateOfBirth = Convert.ToDateTime(model.DateOfBirth);
            int _gender = Convert.ToInt32(model.Gender);

            if (_pesel.Length != 11)
                return new ValidationResult("Numer PESEL musi mieć dokładnie 11 znaków.");

            if (!ValidateGender(_pesel, _gender))
                return new ValidationResult("Błędny numer PESEL. Niepoprawna cyfra płci (10).");

            if (!ValidateDateOfBirth(_pesel, _dateOfBirth))
                return new ValidationResult("Błędny numer PESEL. Niepoprawne cyfry daty urodzenia (1-6).");

            if (!ValidateChecksum(_pesel))
                return new ValidationResult("Błędny numer PESEL. Niepoprawna cyfra kontrolna (11).");

            return ValidationResult.Success;
        }

        /// <summary>
        /// Sprawdza, czy oznaczenie płci w PESEL-u jest prawidłowe.
        /// </summary>
        /// <param name="pesel">Numer PESEL.</param>
        /// <param name="gender">Kod płci - 0 dla mężczyzny, 1 dla kobiety.</param>
        /// <returns>true jeżeli oznaczenie jest prawidłowe, false - jeżeli nie.</returns>
        private bool ValidateGender(string pesel, int gender)
        {
            if (1 - (pesel[9] % 2) == gender)
                return true;
            return false;
        }

        /// <summary>
        /// Sprawdza, czy oznaczenie daty urodzenia w PESEL-u jest prawidłowe.
        /// </summary>
        /// <param name="pesel">Numer PESEL.</param>
        /// <param name="dateOfBirth">Data urodzenia.</param>
        /// <returns>true jeżeli oznaczenie jest prawidłowe, false - jeżeli nie.</returns>
        private bool ValidateDateOfBirth(string pesel, DateTime dateOfBirth)
        {
            string year = dateOfBirth.ToString("yy");
            string day = dateOfBirth.ToString("dd");
            string month;
            if (dateOfBirth.Year >= 1900 && dateOfBirth.Year <= 1999)
                month = dateOfBirth.ToString("MM");
            else if (dateOfBirth.Year >= 2000 && dateOfBirth.Year <= 2099)
                month = (dateOfBirth.Month + 20).ToString();
            else if (dateOfBirth.Year >= 2100 && dateOfBirth.Year <= 2199)
                month = (dateOfBirth.Month + 40).ToString();
            else
                month = "00";

            if (year.Equals(pesel.Substring(0, 2)) && month.Equals(pesel.Substring(2, 2)) && day.Equals(pesel.Substring(4, 2)))
                return true;
            return false;
        }

        /// <summary>
        /// Sprawdza, czy cyfra kontrolna w numerze PESEL jest prawidłowa.
        /// </summary>
        /// <param name="pesel">Numer PESEL.</param>
        /// <returns>true jeżeli oznaczenie jest prawidłowe, false - jeżeli nie.</returns>
        private bool ValidateChecksum(string pesel)
        {
            int control = (int)char.GetNumericValue(pesel[10]);

            int[] pN = pesel.Select(s => (int)char.GetNumericValue(s)).ToArray();

            int checkNumber = pN[0] * 9 + pN[1] * 7 + pN[2] * 3 + pN[3] + pN[4] * 9 +
                pN[5] * 7 + pN[6] * 3 + pN[7] + pN[8] * 9 + pN[9] * 7;

            if (control == checkNumber % 10)
                return true;
            return false;
        }
    }
}